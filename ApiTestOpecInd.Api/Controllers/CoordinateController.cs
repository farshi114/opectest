﻿using System;
using System.Linq;
using ApiTestOpecInd.Core.DTOs;
using ApiTestOpecInd.Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiTestOpecInd.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CoordinateController : ControllerBase
    {
        private readonly ILogService _logService;

        public CoordinateController(ILogService logService)
        {
            _logService = logService;
        }

        [HttpGet]
        public IActionResult GetLogs()
        {
            var logs = _logService.GetFullLogs();
            var logsDto = logs?.Select(c => new LogDto().SaveTo(c)).ToList();
            return Ok(logsDto);
        }


        [HttpPost("CalculateCoordinate")]
        public IActionResult CalculateCoordinate([FromBody] CoordinateDto register)
        {
            //Create Log
            var userId = Convert.ToInt32(User.Identity.Name);
            var newLog = register.LoadFrom(userId);
            _logService.Insert(newLog);

            var distance = new CoordinateDistance();
            distance.SaveTo(newLog);

            return Ok(distance);
        }
    }
}