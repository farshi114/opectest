﻿using System;
using System.Collections.Generic;
using System.Text;
using GeoCoordinatePortable;

namespace ApiTestOpecInd.Core.Helpers
{
    public static class CoordinationHelper
    {
        public static double CalculateDistance(double sLatitude, double sLongitude, double eLatitude,
            double eLongitude)
        {
            var sCoord = new GeoCoordinate(sLatitude, sLongitude);
            var eCoord = new GeoCoordinate(eLatitude, eLongitude);

            return sCoord.GetDistanceTo(eCoord);
        }
    }
}
