﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ApiTestOpecInd.Core.Helpers;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.DTOs
{
    public class CoordinateDto
    {
        [Required]
        public double FirstLongitude { get; set; }

        [Required]
        public double FirstLatitude { get; set; }

        [Required]
        public double SecondLongitude { get; set; }

        [Required]
        public double SecondLatitude { get; set; }

        public Log LoadFrom(int userId)
        {
            return new Log
            {
                DateTime = DateTime.Now,
                FirstLongitude = this.FirstLongitude,
                FirstLatitude = this.FirstLatitude,
                SecondLongitude = this.SecondLongitude,
                SecondLatitude = this.SecondLatitude,
                UserId = userId
            };
        }
    }

    public class CoordinateDistance
    {
        public double Distance { get; set; }

        public void SaveTo(Log log)
        {
            this.Distance = CoordinationHelper.CalculateDistance(log.FirstLatitude, log.FirstLongitude,
                log.SecondLatitude, log.SecondLongitude);
        }
    }

    public class LogDto
    {
        public double FirstLongitude { get; set; }

        public double FirstLatitude { get; set; }

        public double SecondLongitude { get; set; }

        public double SecondLatitude { get; set; }

        public string Email { get; set; }

        public int UserId { get; set; }

        public LogDto SaveTo(Log log)
        {
            this.FirstLongitude = log.FirstLongitude;
            this.FirstLatitude = log.FirstLatitude;
            this.SecondLongitude = log.SecondLongitude;
            this.SecondLatitude = log.SecondLatitude;
            this.Email = log.User?.Email;
            this.UserId = log.UserId;
            return this;
        }
    }
}
