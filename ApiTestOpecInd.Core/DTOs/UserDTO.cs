﻿using System.ComponentModel.DataAnnotations;
using ApiTestOpecInd.Core.Security;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.DTOs
{
    public class AuthenticateDto
    {
        [Required] [EmailAddress] public string Email { get; set; }

        [Required] public string Password { get; set; }

        public User LoadFrom()
        {
            return new User
            {
                Email = this.Email,
                Password = PasswordHelper.EncodePasswordMd5(this.Password)
            };
        }
    }

    public class AuthenticatedUserDto
    {
        public string Email { get; set; }

        public string Token { get; set; }

        public void SaveTo(User user, string token)
        {
            this.Email = user.Email;
            this.Token = token;
        }
    }
}
