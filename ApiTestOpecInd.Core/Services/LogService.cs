﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApiTestOpecInd.Core.Services.Interfaces;
using ApiTestOpecInd.DataLayer.Context;
using ApiTestOpecInd.DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApiTestOpecInd.Core.Services
{
    public class LogService : GenericService<Log>, ILogService
    {
        private readonly TestDbContext _context;
        public LogService(TestDbContext context) : base(context)
        {
            _context = context;

        }

        public List<Log> GetFullLogs()
        {
            return _context.Logs.Include(c => c.User).ToList();
        }
    }
}
