﻿using System;
using System.Collections.Generic;
using System.Text;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.Services.Interfaces
{
    public interface IUserService : IGenericService<User>
    {
        User Authenticate(string email, string password);

        bool IsDuplicateEmail(string email);
    }
}
