﻿using System;
using System.Collections.Generic;
using System.Text;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.Services.Interfaces
{
    public interface IGenericService<TEntity> where TEntity : Entity
    {
        IEnumerable<TEntity> Get();

        TEntity Get(int id);

        TEntity Insert(TEntity entity);

        TEntity Update(TEntity entityToUpdate);

        bool Remove(TEntity entityToDelete);
    }
}
