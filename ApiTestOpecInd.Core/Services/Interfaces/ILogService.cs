﻿using System;
using System.Collections.Generic;
using System.Text;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.Services.Interfaces
{
    public interface ILogService : IGenericService<Log>
    {
        List<Log> GetFullLogs();
    }
}
