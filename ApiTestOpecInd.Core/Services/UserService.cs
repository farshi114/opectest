﻿using System.Linq;
using ApiTestOpecInd.Core.Security;
using ApiTestOpecInd.Core.Services.Interfaces;
using ApiTestOpecInd.DataLayer.Context;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.Services
{
    public class UserService : GenericService<User>, IUserService
    {
        private readonly TestDbContext _context;
        public UserService(TestDbContext context) : base(context)
        {
            _context = context;
            
        }

        public User Authenticate(string email, string password)
        {
            var encPassword = PasswordHelper.EncodePasswordMd5(password);
            var user = _context.Users.SingleOrDefault(x => x.Email == email && x.Password == encPassword);

            return user;
        }

        public bool IsDuplicateEmail(string email)
        {
            return _context.Users.Any(c => c.Email == email);
        }
    }
}
