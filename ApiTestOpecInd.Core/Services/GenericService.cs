﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApiTestOpecInd.Core.Services.Interfaces;
using ApiTestOpecInd.DataLayer.Context;
using ApiTestOpecInd.DataLayer.Entities;

namespace ApiTestOpecInd.Core.Services
{
    public class GenericService<TEntity> : IGenericService<TEntity> where TEntity : Entity
    {
        private readonly TestDbContext _context;

        public GenericService(TestDbContext context)
        {
            _context = context;
        }
        public IEnumerable<TEntity> Get()
        {
            return _context.Set<TEntity>().ToList();
        }

        public TEntity Get(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public TEntity Insert(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public TEntity Update(TEntity entityToUpdate)
        {
            _context.Set<TEntity>().Update(entityToUpdate);
            _context.SaveChanges();
            return entityToUpdate;
        }

        public bool Remove(TEntity entityToDelete)
        {
            _context.Set<TEntity>().Remove(entityToDelete);
            _context.SaveChanges();
            return true;
        }
    }
}
