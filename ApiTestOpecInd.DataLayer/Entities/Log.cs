﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ApiTestOpecInd.DataLayer.Entities
{
    public class Log : Entity
    {
        [Required]
        public double FirstLongitude { get; set; }

        [Required]
        public double FirstLatitude { get; set; }//Width

        [Required]
        public double SecondLongitude { get; set; }

        [Required]
        public double SecondLatitude { get; set; }//Width

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public int UserId { get; set; }

        #region Relation
        [ForeignKey("UserId")]
        public User User { get; set; }

        #endregion
    }
}
