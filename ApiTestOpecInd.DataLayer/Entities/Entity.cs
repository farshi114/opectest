﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApiTestOpecInd.DataLayer.Entities
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
