﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApiTestOpecInd.DataLayer.Entities
{
    public class User : Entity
    {
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [MaxLength(100)]
        public string Password { get; set; }

        #region Relations

        public List<Log> Logs { get; set; }

        #endregion
    }
}
